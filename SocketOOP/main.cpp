#include <iostream>
#include "Socket.h"

#define SERVER_IP "10.0.0.4"
#define SERVER_PORT 1105
#define MAX_CONNECTIONS 10
#define BUFFER_LEN 1024

using std::cout;
using std::endl;

int main(void)
{
	// Creating the server.
	Socket server;
	
	// Bind & start listening.
	server.Bind(SocketAddress(SERVER_IP, SERVER_PORT));
	server.Listen(MAX_CONNECTIONS);
	
	// Wait for a connection.
	Socket* client = server.Accept();

	// Send a message to the client.
	client->Send("Hello from server...");

	// Receive a response from the client.
	string res = client->Receive(BUFFER_LEN);

	cout << res << endl;

	system("pause");
	return 0;
}