#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using std::ostream;
using std::string;
using std::vector;
using std::stringstream;
using std::pair;

class Row
{
public:
	Row();
	~Row();

	void addElement(string& column, string& value);

	string& operator[](const string& column);

	string toString(void);

private:
	vector<pair<string, string>> elements;
};