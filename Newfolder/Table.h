#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "Row.h"

using std::ostream;
using std::vector;
using std::string;
using std::endl;
using std::stringstream;

class Table
{
public:
	Table();
	~Table();

	void addColumn(const string& column);
	void addRow(Row& row);

	vector<string> getColumns(void) const;
	vector<Row> getRows(void) const;

	Row& operator[](int i);

	string toString(void);

private:
	vector<string> columns;
	vector<Row> rows;
};